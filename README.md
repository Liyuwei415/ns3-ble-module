# WARNING

This module will no longer be maintained. The original creation was part of a master-thesis study, performed in 2017-2018.

The module is provided as-is without any guarantees.

# BUILD INSTRUCTIONS
Verified with [ns-3-dev](https://gitlab.com/nsnam/ns-3-dev) on 2021-06-14 with commit [25e0d01d](https://gitlab.com/nsnam/ns-3-dev/-/commit/25e0d01d2883ae6da3c18a4d926f6b9aa2b99128) 

lyw:Verified with ns-3.36.1 on 2022-11-03
## Setup
Clone this repository

```
git clone https://gitlab.com/Liyuwei415/ns3-ble-module.git
```

Clone ns-3-dev and checkout the desired version

```
git clone https://gitlab.com/nsnam/ns-3-dev.git

git checkout 7004d1a66e9b54b6d9dfd6938e2d9323f28942d8

```

Place the `ble` folder of the ns3-ble-module inside the `src` folder of the ns-3-dev repo. This can be done with a copy or a symlink:

```
cd /usr/ns3/src
ln -s -v ../../ns3-ble-module/ble/ ble
```

## Configure and build
Configure ns3 with the desired options using the `waf` build system from the ns-3-dev root:

```
cd ns-3-dev
./ns3 configure 
```

followed by a build command
```
./ns3 build
```

## Run
You can run examples with `--run`

```
./ns3 --run ble-routing-dsdv
```

